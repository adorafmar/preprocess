#! /home/arm/.rbenv/shims/ruby

require 'pry'
require 'csv'

files = Dir["historian/*/*"]
files.each do |file|
	puts file
	CSV.open("out/"+File.basename(file), "w") do |csv|
		csv << ["Time", "Tag", "Value"]
		table = CSV.parse(File.read(file), headers: true)
		table.each do |row|
			if row["Time"].nil? then
				puts file + ": nil"
			else
				if row["Time"].include? "/" then
					computer_time = Date.strptime(row["Time"], "%m/%d/%Y")
					row["Time"] = computer_time.to_s
					csv << row
				elsif row["Time"].include? "-" then
					computer_time = Date.strptime(row["Time"], "%Y-%m-%d")
					row["Time"] = computer_time.to_s
					csv << row
				end
			end
		end
	end
end
