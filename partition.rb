#! /home/arm/.rbenv/shims/ruby

require 'pry'
require 'fileutils'

files = Dir["out/*"]
files.each do |file|
	puts file
	dir_name = File.basename(file).split("_")[0]
	begin
		Dir.mkdir("partitioned/"+dir_name)
	rescue
	end
	FileUtils.mv(file, "partitioned/"+dir_name+"/"+File.basename(file))
end